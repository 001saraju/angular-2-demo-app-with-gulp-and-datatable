"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var data_service_1 = require("../services/data-service");
var items_service_1 = require("../services/items-service");
var AppComponent = (function () {
    function AppComponent(http, dataService, itemsService) {
        this.http = http;
        this.dataService = dataService;
        this.itemsService = itemsService;
        this.filterQuery = "";
        this.rowsOnPage = 10;
        this.sortBy = "email";
        this.sortOrder = "asc";
        this.sortByWordLength = function (a) {
            return a.city.length;
        };
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dataService.getData()
            .subscribe(function (data) {
            _this.data = data;
        }, function (error) {
            alert('erro - ' + error);
        });
    };
    AppComponent.prototype.remove = function (item, indx) {
        this.itemsService.removeItemFromArray(this.data, item);
        //this.data.splice(indx, 1);
    };
    AppComponent.prototype.toInt = function (num) {
        return +num;
    };
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        selector: 'my-app',
        //template: `<h1>Hello {{name}}:{{amount}}</h1>`,
        templateUrl: 'app.component.html',
        styleUrls: ['styles.css']
    }),
    __metadata("design:paramtypes", [http_1.Http, data_service_1.DataService, items_service_1.ItemsService])
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map