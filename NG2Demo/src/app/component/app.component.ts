﻿import { Component,OnInit} from "@angular/core";
import { Http } from "@angular/http";
import { DataService } from '../services/data-service';
import { IDetail } from '../models/IDetail';
import { ItemsService } from '../services/items-service';

@Component({
    moduleId: module.id,
    selector: 'my-app',
    //template: `<h1>Hello {{name}}:{{amount}}</h1>`,
    templateUrl:'app.component.html',
    styleUrls: ['styles.css']
})
export class AppComponent implements OnInit {
    public data;
    public filterQuery = "";
    public rowsOnPage = 10;
    public sortBy = "email";
    public sortOrder = "asc";

    constructor(private http: Http, private dataService: DataService, private itemsService: ItemsService) {
    }

    ngOnInit(): void {
        this.dataService.getData()
            .subscribe((data: IDetail[]) => {
                    this.data = data;
            },
            error => {
                alert('erro - ' + error);
            });
    }
    remove(item: IDetail, indx: number)
    {
        this.itemsService.removeItemFromArray<IDetail>(this.data, item);
        //this.data.splice(indx, 1);
    }
    public toInt(num: string) {
        return +num;
    }

    public sortByWordLength = (a: any) => {
        return a.city.length;
    }
}